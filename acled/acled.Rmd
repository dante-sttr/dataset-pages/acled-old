---
title: "The Armed Conflict Location & Event Data Project (ACLED)"

date: "`r Sys.Date()`"

submission-contact:
  - name: Joshua Brinks
    email: jbrinks@isciences.com
    affiliation: ISciences, L.L.C.

dataset-author: 
  - name: Clionadh Raleigh
    email: raleighc@tcd.ie
    affiliation: Department of Geography, Trinity College Dublin, CSCW, Peace Research Institute Oslo (PRIO)
  - name: Andrew Linke
    affiliation: Department of Geography, University of Colorado
  - name: Håvard Hegre
    affiliation: Department of Political Science, University of Oslo, CSCW, Peace Research Institute Oslo (PRIO)
  - name: Joakim Karlsen
    affiliation: Østfold University College, CSCW, Peace Research Institute Oslo (PRIO)


abstract: "This article presents ACLED, an Armed Conflict Location and Event Dataset. ACLED codes the actions of rebels, governments, and militias within unstable states, specifying the exact location and date of battle events, transfers of military control, headquarter establishment, civilian violence, and rioting. In the current version, the dataset covers 50 unstable countries from 1997 through 2010. ACLED’s disaggregation of civil war and transnational violent events allow for research on local level factors and the dynamics of civil and communal conflict. Findings from subnational conflict research challenges conclusions from larger national-level studies. In a brief descriptive analysis, the authors find that, on average, conflict covers 15% of a state’s territory, but almost half of a state can be directly affected by internal wars."

dataset-host: https://www.acleddata.com/
    
datatypes:
  - spatial point
  - dyadic
  
spatial-information:
  extent: global
  resolution: point
  coordinate-system: Lat/Long

temporal-information:
  start-year: 1997
  end-year: 2019
  time-step: daily

related-packages:
  - conflictr

vignettes:
  - Related Dante Vignette  

pros:
  - Spatially explicit and disaggregated point locations for battle activities.
  - Includes non-violent battle events (troop movements, diplomacy, etc.)

cons:
  - Violent and non-violent events are not separated.
  - Loose or non-existent actor definitions.
  - Less precision and accuracy in geo-referencing.
  
citekey: ClionadhRaleigh2010

bibliography: "/home/joshb/dante/common/DANTE-Project Citations.bib"

output: 
  danteSubmit::danteDataset
  
---

### Discussion:

The Armed Conflict Location & Event Data Project (ACLED; @Raleigh2010) is one of a number of recently developed high disaggregated armed conflict datasets. Similar to The Uppsala Conflict Data Program Georeferenced Event Dataset (UCDP-GED; v18.1; @Sundberg2013), The Social Conflict Analysis in Africa Database (SCAD; @Salehyan2012), and the Militarized Interstate Dispute Locations (MIDL v4.3; @Maoz2018) datasets, ACLED presents dyadic conflict data with a high level of spatial accuracy. Although these datasets were initially released with restricted geographic scope (with the exception of MIDL), they all expanded their spatial reach significantly over the past 5-10 years. ACLED now encompasses Africa, South Asia, South East Asia, the Middle East, Europe, and Latin America, UCDP-GED is currently advertised as a global dataset of armed conflict, and SCAD has expanded to include Africa, Mexico, Central America, and the Caribbean. 

These datasets are similar in nature, but can vary greatly in their reporting criteria and supplementary information. @Eck2012 recently reviewed the fundamental differences between UCDP-GED and ACLED. UCDP-GED provides superior data for violence and fatalaties, but ACLED distinguishes itself by being the only source for non-violent conflict related events like troop movements and governmental aid. Furthermore, in contrast to UCDP-GED, ACLED uses less restrictive definitions of "events" and "actors" involved in an event. The effect of non-violent event coding and looser actor definitions is a larger database, however, it may be less suitable for statistical inference and analysis without substantial pre-processing. Spatially disaggregated conflict data presents additional challenges. Larger conflicts between the same actors often result in several battle events that are recorded as individual observations. Although UCDP-GED provides several metadata fields and additional datasets to better characterize the complexities of inter and intranational conflict, ACLED provides limited metadata and no additional datasets.


### Screenshot or Representative Figure:

This may also be developed from a code chunk:

```{r message=FALSE, cache = TRUE, warning=FALSE, dev='svg', fig.cap="Figure 2: Weight as a function of height for a subset of 15 women. Blue line represents linear relationship with 95% confidence interval (grey bands)."}

library(ggplot2)
library(conflictr)

getACLED(country = "Sudan")
women.scatter<-ggplot(women,aes(x=height,y=weight))+
  geom_point(size=3)+
  stat_smooth(method="lm", formula="y~x")+
  labs(title="Weight as a Function of Height for 15 Women",
       x="Height (Inches)",
       y="Weight (lbs.)")+
  theme_minimal()
women.scatter
  
```


### Citation:

@ClionadhRaleigh2010

### Additional Submission Comments:

These are some additional comments I would like to add that were not addressed above.

# Reference
